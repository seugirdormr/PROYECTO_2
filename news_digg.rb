require 'json'
require 'rest-client'

class NewsDigg < News

  def self.sort_news
    @news = []
    base_hash = JSON.parse RestClient.get(URL_DIGG)
    base_hash_new = base_hash["data"]["feed"]
    base_hash_new.each { |new| 
      @news << NewsDigg.new( "#{new['content']['title_alt']}", "#{new['content']['author']}", "#{new['date_published']}", "#{new['content']['original_url']}" )
      @@news_all << NewsDigg.new( "#{new['content']['title_alt']}", "#{new['content']['author']}", "#{new['date_published']}", "#{new['content']['original_url']}" )
    }
  end

end