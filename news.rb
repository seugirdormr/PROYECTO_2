require 'json'
require 'rest-client'

class News
  @@news_all = []
  attr_accessor :title, :author, :date, :link
  def initialize (title, author, date, link)
    @title = title
    @author =author
    @date = date
    @link = link
  end

  def self.sort_news
    @news = []
    base_hash = JSON.parse RestClient.get(URL_MASHABLE)
    base_hash_new = base_hash["new"]
    base_hash_new.each { |new| 
      @news << News.new( "#{new['title']}", "#{new['author']}", "#{new['post_date']}", "#{new['link']}" )
      @@news_all << News.new( "#{new['title']}", "#{new['author']}", "#{new['post_date']}", "#{new['link']}" )
    }
  end

  def self.show_all
      @@news_all.each{ |new|
        puts "Title: #{new.title}"
        puts
        puts "Author: #{new.author}"
        puts      
        puts "Date: #{new.date}"
        puts      
        puts "Link: #{new.link}" 
        puts
        puts
        puts '                            Press enter for next new'
        gets
      }
    system ('clear')
  end

  def self.show_news
    @news.each{ |new|
      system ('clear')
      puts "Title: #{new.title}"
      puts
      puts "Author: #{new.author}"
      puts      
      puts "Date: #{new.date}"
      puts      
      puts "Link: #{new.link}" 
      puts
      puts
      puts '                            Press enter for next new'
      gets
    }
    system ('clear')
  end
end