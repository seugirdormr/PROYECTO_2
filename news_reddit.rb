require 'json'
require 'rest-client'
require 'date'

class NewsReddit < News
  def self.sort_news
    @news = []
    base_hash = JSON.parse RestClient.get(URL_REDDIT)
    base_hash_new = base_hash["data"]["children"]
    base_hash_new.each { |new| 
      @news << NewsReddit.new( "#{new['data']['title']}", "#{new['data']['author']}", "#{new['data']['created']}", "#{new['data']['url']}" )
      @@news_all << NewsReddit.new( "#{new['data']['title']}", "#{new['data']['author']}", "#{new['data']['created']}", "#{new['data']['url']}" )
    }
  end

end