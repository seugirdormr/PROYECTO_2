require_relative 'news'
require_relative 'news_reddit'
require_relative 'news_digg'

class Merge
  def self.merge_websites
    @news = []
    base_hash = JSON.parse RestClient.get(URL_MASHABLE)
    base_hash_new = base_hash["new"]
    base_hash_new.each { |new| 
      @news << News.new( "#{new['title']}", "#{new['author']}", "#{new['post_date']}", "#{new['link']}" )
    }
    base_hash = JSON.parse RestClient.get(URL_REDDIT)
    base_hash_new = base_hash["data"]["children"]
    base_hash_new.each { |new| 
      @news << NewsReddit.new( "#{new['data']['title']}", "#{new['data']['author']}", "#{new['data']['created']}", "#{new['data']['url']}" )
    }
    base_hash = JSON.parse RestClient.get(URL_DIGG)
    base_hash_new = base_hash["data"]["feed"]
    base_hash_new.each { |new| 
      @news << NewsDigg.new( "#{new['content']['title_alt']}", "#{new['content']['author']}", "#{new['date_published']}", "#{new['content']['original_url']}" )
    }
  end

  def self.show_merge
    @news.each{ |new|
      puts "Title: #{new.title}"
      puts
      puts "Author: #{new.author}"
      puts      
      puts "Date: #{new.date}"
      puts      
      puts "Link: #{new.link}" 
      puts
      puts
      puts '                            Press enter for next new'
      gets
    }
    system ('clear')
  end
end